#!/bin/bash

PKG_VERSION=$(head -n1 packaging/debian/changelog | sed 's/snoop (//' | cut -d- -f1)
PKG_VERSION=0.1

PKG_FOLDER=deb-package/snoop-$PKG_VERSION

echo version: $PKG_VERSION

mkdir -p $PKG_FOLDER

cp -r packaging/debian $PKG_FOLDER
tar cfJ $PKG_FOLDER/../snoop_$PKG_VERSION.orig.tar.xz src/* build-aux/meson/* extension/* data/* po/* meson.build

cd $PKG_FOLDER
tar xf ../snoop_$PKG_VERSION.orig.tar.xz

debuild -uc -us
