/* window.vala
 *
 * Copyright 2021 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  public class Grepper
  {
    private bool proc_running = false;
    private bool done = true;
    private bool killed = false;
    private Thread<void> thread;
    private Pid pid;
    private List<string> lines = new List<string>();
    private IOChannel channel_output;

    public signal void finished();
    public signal void line_processed(owned Array<Item> items);

    public Grepper ()
    {
    }

    ~Grepper()
    {
      int i = 0;
    }

    public bool is_running()
    {
      return proc_running || done == false;
    }

    public void start(string search, string path, bool case_sensitive, bool whole_words, bool regexp, bool ignore_hidden)
    {
      if (thread != null)
      {
        thread.join();
      }

      proc_running = false;
      done = false;
      killed = false;

      string[] app_string_full = { "/bin/sh", "-c" };
      string app_string = "";

      app_string += "grep";
      if (regexp == false)
      {
        app_string += " -F";
      }
      app_string += " -r";
      app_string += " -n";

      if (case_sensitive == false)
      {
        app_string += " -i";
      }

      if (whole_words)
      {
        app_string += " -w";
      }

      if (ignore_hidden)
      {
        app_string += " --exclude=\".*\"";
        app_string += " --exclude-dir=\".*\"";
      }

      var escaped_search = search.replace("\"", "\\\"");
      app_string += " \"" + escaped_search + "\" ";
      app_string += " \"" + path + "\"";

      app_string_full += app_string;

      int standard_output;

      try
      {
        proc_running = Process.spawn_async_with_pipes (path,
          app_string_full,
          null,
          SpawnFlags.STDERR_TO_DEV_NULL | SpawnFlags.SEARCH_PATH,
          null,
          out pid,
          null,
          out standard_output,
          null);

        channel_output = new IOChannel.unix_new (standard_output);
        channel_output.add_watch (IOCondition.HUP, gio_in);
      }
      catch (GLib.SpawnError e)
      {
      }

      thread = new Thread<void>("grepper", () =>
      {
        while(proc_running)
        {
          read_buffer();

          process_lines();
        }

        done = true;
        finished();
      });
    }

    public void stop()
    {
      killed = true;
      Process.spawn_sync(null, {"kill", pid.to_string()}, null, SpawnFlags.SEARCH_PATH, null, null, null, null);
      Process.close_pid (pid);
    }

    private bool gio_in (IOChannel channel, IOCondition condition)
    {
      if (condition == IOCondition.HUP)
      {
        proc_running = false;
        return false;
      }

      return true;
    }

    private void read_buffer()
    {
      size_t size = 0;

      // var condition = channel_output.get_buffer_condition();
      // todo condition is always 0 outside of the watcher, so we just try to read from the IOChannel
      // print("condition: %d, IN: %d, OUT: %d, PRI: %d, PRI: %d, HUP: %d, NVAL: %d\n", condition,
      //   IOCondition.IN,
      //   IOCondition.OUT,
      //   IOCondition.PRI,
      //   IOCondition.HUP,
      //   IOCondition.ERR,
      //   IOCondition.NVAL);
      uint count = 0;
      while (killed == false && count < 10000)
      {
        count++;
        try
        {
          string line;
          channel_output.read_line (out line, out size, null);
          if (size > 0)
          {
            // some files may have \r as well, remove it
            line = line.replace("\r", "");
            lines.append(line);
          }
          else
          {
            Thread.usleep(10000);
          }
        }
        catch (IOChannelError e)
        {
          stdout.printf ("IOChannelError: %s\n", e.message);
          break;
        }
        catch (ConvertError e)
        {
          stdout.printf ("ConvertError: %s\n", e.message);
          break;
        }

        if (proc_running == false && size == 0)
        {
          break;
        }

        // condition = channel_output.get_buffer_condition();
      }

      // if (condition == IOCondition.HUP || killed)
      if (killed)
      {
        proc_running = false;
      }
    }

    private void process_lines()
    {
      Array<Item> items = new Array<Item>.sized(false, true, sizeof(Item), lines.length());
      foreach (var line in lines)
      {
        var item = process_line(line);
        if (item != null)
          items.append_val(item);
      }

      if (items.length > 0)
      {
        line_processed((owned) items);
      }

      lines = new List<string>();
    }

    private Item? process_line(string line)
    {
      if (line.length > 0 && line[0] == '/')
      {
        var parts = line.split(":", 3);
        if (parts.length == 3)
        {
          var text = parts[2].substring(0, parts[2].length - 1 /* skip newline of grep output */);

          return new Item() { text = text, line = parts[1], file = parts[0]};
        }
      }

      return null;
    }

  }
}
