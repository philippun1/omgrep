/* found-items-model.vala
 *
 * Copyright 2021 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  public class Item : GLib.Object
  {
    public string text;
    public string line;
    public string file;
  }

  public class FoundItemsModel : GLib.Object, GLib.ListModel
  {
    // list of items in the model
    private List<Array<Item>> list_of_items = new List<Array<Item>>();

    public FoundItemsModel ()
    {
    }

    public void add_items(owned Array<Item> new_items)
    {
      var position = get_n_items() - 1;
      var new_length = new_items.length;

      list_of_items.append((owned) new_items);

      items_changed(position, 0, new_length);
    }

    public void clear()
    {
      var count = get_n_items();

      list_of_items = new List<Array<Item>>();

      items_changed(0, count, 0);
    }

    public uint get_n_items()
    {
      uint l = 0;

      foreach (var items in list_of_items)
      {
        l += items.length;
      }

      return l;
    }

    public GLib.Object? get_item(uint position)
    {
      Item? item = null;

      foreach (var items in list_of_items)
      {
        if (position < items.length)
        {
          item = items.index(position);
          break;
        }
        else
        {
          position -= items.length;
        }
      }

      return item;
    }

    public GLib.Type get_item_type()
    {
      return typeof(Item);
    }
  }
}

