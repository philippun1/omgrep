/* preview.vala
 *
 * Copyright 2024 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  [GtkTemplate (ui = "/de/philippun1/Snoop/ui/preview.ui")]
  public class Preview : Adw.Window
  {

    [GtkChild]
    private unowned Gtk.ScrolledWindow scrolled_window;

    private GtkSource.View source_view;
    private int linenumber = 0;

    public Preview (Gtk.Window? parent)
    {
      source_view = new GtkSource.View();
      source_view.set_highlight_current_line(true);
      source_view.set_show_line_numbers(true);
      scrolled_window.set_child(source_view);

      install_key_filter();
    }

    private void set_style()
    {
      var style_manager = Adw.StyleManager.get_default();
      var scheme_manager = GtkSource.StyleSchemeManager.get_default();

      var is_dark = style_manager.get_dark();

      var name = "Adwaita";
      if (is_dark)
      {
        name += "-dark";
      }

      var scheme = scheme_manager.get_scheme(name);
      var buffer = source_view.get_buffer() as GtkSource.Buffer;
      buffer.set_style_scheme(scheme);
    }

    private void install_key_filter()
    {
      var controller = new Gtk.EventControllerKey();
      controller.key_pressed.connect(key_pressed_handler);
      ((Gtk.Widget) this).add_controller(controller);
    }

    private bool key_pressed_handler(Gtk.EventControllerKey controller, uint keyval, uint keycode, Gdk.ModifierType state)
    {
      if (keyval == Gdk.Key.Escape)
      {
        close();
      }

      return false;
    }

    public void set_file(string path, int linenumber)
    {
      this.linenumber = linenumber;

      var file = GLib.File.new_for_path(path);
      var relative = file.get_basename();

      set_title(relative);

      string text = "";

      try {
        FileInputStream @is = file.read ();
        DataInputStream dis = new DataInputStream (@is);
        string line;

        while ((line = dis.read_line ()) != null)
        {
          text += line + "\n";
          stdout.flush();
        }
      }
      catch (Error e)
      {
        print ("Error: %s\n", e.message);
      }

      var buffer = source_view.get_buffer() as GtkSource.Buffer;
      buffer.set_text(text);

      var manager = new GtkSource.LanguageManager();
      var language = manager.guess_language(path, null);
      buffer.set_language(language);

      set_style();

      show.connect(move_to_line);
    }

    public void move_to_line()
    {
      var buffer = source_view.get_buffer();

      var iter = Gtk.TextIter();
      buffer.get_iter_at_line(out iter, linenumber);
      buffer.place_cursor(iter);

      // scrolling directly would be too early
      GLib.Timeout.add(200, () =>
      {
        source_view.scroll_to_iter(iter, 0, true, 0, 0.5);
        return false;
      });
    }

  }
}


