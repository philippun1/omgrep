/* found-items-view.vala
 *
 * Copyright 2021 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  public class FoundItemsView
  {
    private string search = "";
    private bool case_sensitive = false;
    private int limit = 200;

    private const string dot_dot_dot = "<span foreground='gray'>[...]</span>";

    public FoundItemsView(Gtk.ListView? view)
    {
      var factory = new Gtk.SignalListItemFactory();

      factory.setup.connect(setup_cb);
      factory.teardown.connect(teardown_cb);

      factory.bind.connect(bind_cb);
      factory.unbind.connect(unbind_cb);

      view.factory = factory;
    }

    public void set_search_string(string search, bool case_sensitive)
    {
      this.search = search;
      this.case_sensitive = case_sensitive;
    }

    public void setup_cb(Gtk.SignalListItemFactory factory, GLib.Object item)
    {
      var grid = new Gtk.Grid();

      var label_text = new Gtk.Label(null);
      label_text.set_halign(Gtk.Align.START);

      var label_file = new Gtk.Label(null);
      label_file.set_halign(Gtk.Align.START);
      label_file.hexpand = true;

      var label_line = new Gtk.Label(null);
      label_line.set_halign(Gtk.Align.END);

      grid.attach(label_text, 0, 0, 2);
      grid.attach(label_file, 0, 1);
      grid.attach(label_line, 1, 1);
      grid.set_margin_start(6);
      grid.set_margin_end(6);
      grid.set_margin_top(2);
      grid.set_margin_bottom(2);
      grid.set_row_spacing(2);

      var list_item = (Gtk.ListItem)item;
      list_item.set_child(grid);
    }

    public void teardown_cb(Gtk.SignalListItemFactory factory, GLib.Object item)
    {

    }

    public void bind_cb(Gtk.SignalListItemFactory factory, GLib.Object item)
    {
      var list_item = (Gtk.ListItem)item;
      var grid = (Gtk.Grid)list_item.get_child();
      var it = (Item)list_item.get_item();

      // text label
      var label_text = (Gtk.Label)grid.get_child_at(0, 0);
      var text = it.text;

      text = higlight_text(text);

      label_text.set_markup(text);

      // file label
      var label_file = (Gtk.Label)grid.get_child_at(0, 1);
      var file = it.file;

      // if (file.length > limit)
      //   file = "[...]" + file.substring(it.file.length - 45, 45);

      label_file.set_markup("<span foreground='gray'>" + file + "</span>");

      // line label
      var label_line = (Gtk.Label)grid.get_child_at(1, 1);
      label_line.set_markup("<span foreground='gray'>" + it.line + "</span>");
    }

    public void unbind_cb(Gtk.SignalListItemFactory factory, GLib.Object item)
    {

    }

    private string higlight_text(string input)
    {
      var output = "";
      var text = input;
      var colored_search = "<span background='#3584e4'>" + search   + "</span>";

      var cut_start = false;
      var cut_end = false;

      if (case_sensitive)
      {
        var pos = text.index_of(search, 0);
        var dummy = "";

        limit_text(search, ref pos, ref text, ref dummy, ref cut_start, ref cut_end);

        text = GLib.Markup.escape_text(text);

        var split = text.split(search);

        foreach (var s in split)
        {

          output += s + colored_search;
        }

        if (output.length > colored_search.length)
        {
          output = output.substring(0, output.length - colored_search.length);
        }
      }
      else
      {
        // case insensitive split is not possible, so implement it
        var lower_text = text.down();
        var lower_search = search.down();

        var pos = lower_text.index_of(lower_search, 0);
        var start = 0;
        var search_length = search.length;

        limit_text(lower_search, ref pos, ref text, ref lower_text, ref cut_start, ref cut_end);

        while (pos > -1)
        {
          var sub_text = text.substring(start, pos - start);

          sub_text = GLib.Markup.escape_text(sub_text);

          output += sub_text;
          output += "<span background='#3584e4'>" + text.substring(pos, search_length) + "</span>";

          start = pos + search_length;
          pos = lower_text.index_of(lower_search, start);
        }

        if (start > -1 && start < text.length)
        {
          var sub_text = text.substring(start);

          sub_text = GLib.Markup.escape_text(sub_text);

          output += sub_text;
        }
      }

      if (cut_start || cut_end)
      {
        output = (cut_start ? dot_dot_dot : "") + output + (cut_end ? dot_dot_dot : "");
      }

      return output;
    }

    private void limit_text(string search, ref int pos, ref string text, ref string lower_text, ref bool cut_start, ref bool cut_end)
    {
      if (text.length > limit + 8)
      {
        var offset = text.length - (pos + limit);
        if (offset > 0)
        {
          offset = 0;
        }

        if (pos + offset > 0)
        {
          cut_start = true;
        }
        if (pos + offset + limit < text.length)
        {
          cut_end = true;
        }

        text = text.substring(pos + offset, limit);

        if (lower_text.length > 0)
        {
          lower_text = lower_text.substring(pos + offset, limit);
          pos = lower_text.index_of(search, 0);
        }
        else
        {
          pos = text.index_of(search, 0);
        }
      }
    }
  }
}

