/* window.vala
 *
 * Copyright 2021 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  [GtkTemplate (ui = "/de/philippun1/Snoop/ui/window.ui")]
  public class Window : Adw.ApplicationWindow
  {
    [GtkChild]
    private unowned Adw.Clamp settings_clamp;
    [GtkChild]
    private unowned Gtk.Switch match_case;
    [GtkChild]
    private unowned Gtk.Switch whole_words;
    [GtkChild]
    private unowned Gtk.Switch ignore_hidden;
    [GtkChild]
    private unowned Gtk.Switch regular_expressions;
    [GtkChild]
    private unowned Adw.ExpanderRow settings_row;
    [GtkChild]
    private unowned Gtk.Button search_button;
    [GtkChild]
    private unowned Gtk.Button cancel_button;
    [GtkChild]
    private unowned Gtk.Entry search_entry;
    [GtkChild]
    private unowned Gtk.Entry search_path_entry;
    [GtkChild]
    private unowned Adw.SplitButton folder_splitbutton;
    [GtkChild]
    private unowned Gtk.ListView found_items_view;
    [GtkChild]
    private unowned Adw.ToastOverlay toaster;
    [GtkChild]
    private unowned Gtk.MenuButton recent_entries_button;

    private FoundItemsView found_items_view_wrapper;
    private FoundItemsModel found_items;
    private uint64 item_count = 0;
    private bool killed = false;

    private Settings settings;
    private Menu folder_menu;
    private Menu entries_menu;

    private Gtk.FileChooserNative file_chooser;

    private Grepper grepper;

    private Adw.Toast toast;
    private bool status_running = false;
    private uint status_counter = 0;


    public Window (Gtk.Application app)
    {
      Object (application: app);
      grepper = new Grepper();
      grepper.line_processed.connect(add_lines_to_table);
      grepper.finished.connect(grepper_finished);

      found_items_view_wrapper = new FoundItemsView(found_items_view);
      found_items_view.activate.connect(row_activated);

      settings = new Settings("de.philippun1.Snoop");
      add_action(settings.create_action ("simple-view"));
      add_action(settings.create_action ("match-case"));
      add_action(settings.create_action ("whole-words"));
      add_action(settings.create_action ("regular-expressions"));
      settings.bind("simple-view", settings_clamp, "visible", SettingsBindFlags.INVERT_BOOLEAN);
      settings.bind("match-case", match_case, "active", SettingsBindFlags.DEFAULT);
      settings.bind("whole-words", whole_words, "active", SettingsBindFlags.DEFAULT);
      settings.bind("ignore-hidden", ignore_hidden, "active", SettingsBindFlags.DEFAULT);
      settings.bind("regular-expressions", regular_expressions, "active", SettingsBindFlags.DEFAULT);

      settings.changed.connect(settings_updated);
      settings_updated("match-case");

      setup_menus();

      search_entry.changed.connect(search_changed);
      search_path_entry.changed.connect(path_changed);

      // set search string and path from last session
      search_entry.set_text(settings.get_string("search-string"));
      if (Application.parameter_path.length > 0 && is_path_valid(Application.parameter_path))
      {
        search_path_entry.set_text(Application.parameter_path);
      }
      else
      {
        search_path_entry.set_text(settings.get_string("search-path"));
      }

      search_changed(search_entry);
      path_changed(search_path_entry);

      this.close_request.connect(on_close_request);

      found_items = new FoundItemsModel();
      var sorted_items = new Gtk.SingleSelection(found_items);
      found_items_view.set_model(sorted_items);

      // TODO select_region does not work so early, so we need to sleep 200ms
      new Thread<void>(null, () => { Thread.usleep(200000); focus_search_entry(); });
    }

    void focus_search_entry()
    {
      search_entry.grab_focus();
      search_entry.select_region(0, -1);
    }

    bool on_close_request()
    {
      // store recent paths
      var list = "";
      for (var i = 0; i < folder_menu.get_n_items(); i++)
      {
        var path = folder_menu.get_item_attribute_value(i, GLib.Menu.ATTRIBUTE_LABEL, null);
        list += path.get_string() + ":";
      }

      if (list.length > 0)
      {
          list = list.substring(0, list.length - 1);
      }
      settings.set_string("recent-paths", list);

      // store recent entries
      list = "";
      for (var i = 0; i < entries_menu.get_n_items(); i++)
      {
        var entry = entries_menu.get_item_attribute_value(i, GLib.Menu.ATTRIBUTE_LABEL, null);
        list += entry.get_string() + ":";
      }

      if (list.length > 0)
      {
          list = list.substring(0, list.length - 1);
      }
      settings.set_string("recent-entries", list);

      // store current search string
      settings.set_string("search-string", search_entry.get_text());

      // store current search path (if valid path)
      if (is_path_valid(search_path_entry.get_text()))
      {
        settings.set_string("search-path", search_path_entry.get_text());
      }

      found_items_view.set_model(null);
      found_items.clear();

      return false;
    }

    void setup_menus()
    {
      // folder menu
      folder_menu = new Menu();
      var recent_path_action = new SimpleAction("recent-path", new VariantType("s"));
      add_action(recent_path_action);
      recent_path_action.activate.connect((p) =>
      {
        search_path_entry.set_text(p.get_string());
      });
      // append recently searched paths from settings
      var recent_paths = settings.get_string("recent-paths");
      foreach (var path in recent_paths.split(":"))
      {
        add_recent_path(path);
      }

      // entries menu
      entries_menu = new Menu();
      var recent_entry_action = new SimpleAction("recent-entry", new VariantType("s"));
      add_action(recent_entry_action);
      recent_entry_action.activate.connect((p) =>
      {
        search_entry.set_text(p.get_string());
        focus_search_entry();
      });
      // append recently searched entries from settings
      var recent_entries = settings.get_string("recent-entries");
      foreach (var path in recent_entries.split(":"))
      {
        add_recent_entry(path);
      }
    }

    [GtkCallback]
    public void search_clicked()
    {
      if (grepper.is_running())
      {
        grepper.stop();
        killed = true;
        show_toast(_("Grepper was still running. Killed it!"));
      }
      else
      {
        found_items.clear();
        item_count = 0;
        killed = false;

        var search = search_entry.get_text();
        var path = search_path_entry.get_text();
        if (search.length <= 0)
        {
          show_toast(_("Search text must not be empty"));
        }
        else if (path.length <= 0)
        {
          show_toast(_("Search path must not be empty"));
        }
        else
        {
          if (is_path_valid(path))
          {
            found_items_view_wrapper.set_search_string(search, settings.get_boolean("match-case"));

            status_running = true;

            // show indefinite toast
            show_toast(_("Searching...."), 0);

            status_counter = 0;

            GLib.Timeout.add(1000, () =>
            {
              var text = _("Found " + item_count.to_string() + " items");
              status_counter = status_counter % 4;

              if (status_counter == 0)
                update_toast(text + ".   ");
              else if (status_counter == 1)
                update_toast(text + "..  ");
              else if (status_counter == 2)
                update_toast(text + "... ");
              else
                update_toast(text + "....");
              status_counter++;

              if (grepper.is_running() == false)
              {
                update_toast(_("Finished with %s items").printf(item_count.to_string()), 2);
                return false;
              }

              return true;
            });

            expand_path(ref path);
            grepper.start(search, path, settings.get_boolean("match-case"), settings.get_boolean("whole-words"), settings.get_boolean("regular-expressions"), settings.get_boolean("ignore-hidden"));

            update_ui();

            // add valid search path to recent paths
            add_recent_path(path);

            // add search entry to recent entries
            add_recent_entry(search);
          }
          else
          {
            show_toast(_("Path does not exist"));
          }
        }
      }
    }

    [GtkCallback]
    public void cancel_clicked()
    {
      grepper.stop();
      killed = true;
    }

    [GtkCallback]
    public void folder_clicked()
    {
      if (file_chooser == null)
      {
        file_chooser = new Gtk.FileChooserNative(_("Search Path"), this, Gtk.FileChooserAction.SELECT_FOLDER, _("_Open"), _("_Cancel"));
      }
      var path = GLib.File.new_for_path(search_path_entry.get_text());

      if (path.query_exists())
      {
        try
        {
          file_chooser.set_current_folder(path);
        }
        catch (GLib.Error e)
        {
          show_toast(_("Error: %s").printf(e.message));
        }
      }

      file_chooser.response.connect((response) =>
      {
        if (response == Gtk.ResponseType.ACCEPT)
        {
          search_path_entry.set_text(file_chooser.get_file().get_path());
        }
      });

      file_chooser.show();
    }

    public void grepper_finished()
    {
      status_running = false;

      update_ui();

      focus_search_entry();
    }

    public void settings_updated(string key)
    {
      if (key == "match-case" || key == "whole-words" || key == "regular-expressions")
      {
        string prefix = _("Configure your search");
        string text = "";

        if (settings.get_boolean("match-case"))
        {
          text += _("match case");
        }

        if (settings.get_boolean("whole-words"))
        {
          if (text.length > 0)
            text += ", ";
          text += _("whole words");
        }

        if (settings.get_boolean("regular-expressions"))
        {
          if (text.length > 0)
            text += ", ";
          text += _("regex");
        }

        if (text.length > 0)
        {
            text = " (" + text + ")";
        }

        settings_row.set_subtitle(prefix + text);
      }
    }

    void expand_path(ref string path)
    {
      if (path.has_prefix("~"))
      {
        var home = GLib.Environment.get_variable("HOME");
        path = path.replace("~", home);
      }
    }

    bool is_path_valid(string path_to_validate)
    {
      var path = path_to_validate;
      expand_path(ref path);

      return FileUtils.test(path, FileTest.IS_DIR);
    }

    void search_changed(Gtk.Editable entry)
    {
      if (entry.get_text().length > 0)
      {
        search_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, null);
      }
      else
      {
        search_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning-symbolic");
      }

      update_ui();
    }

    void path_changed(Gtk.Editable entry)
    {
      if (is_path_valid(entry.get_text()))
      {
        search_path_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, null);
      }
      else
      {
        search_path_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "dialog-warning-symbolic");
      }

      update_ui();
    }

    private void add_recent_path(string path)
    {
      if (is_path_valid(path))
      {
        for (var i = 0; i < folder_menu.get_n_items(); i++)
        {
          var existing_path = folder_menu.get_item_attribute_value(i, GLib.Menu.ATTRIBUTE_LABEL, null).get_string();
          if (path == existing_path)
          {
              return;
          }
        }
        folder_menu.insert(0, path, "win.recent-path('" + path + "')");
        folder_splitbutton.set_menu_model(folder_menu);

        // limit to 10 items
        if (folder_menu.get_n_items() > 10)
        {
          folder_menu.remove(2);
        }
      }
    }

    private void add_recent_entry(string entry)
    {
      entries_menu.insert(0, entry, "win.recent-entry('" + entry + "')");
      recent_entries_button.set_menu_model(entries_menu);

      // limit to 10 items
      if (entries_menu.get_n_items() > 10)
      {
        entries_menu.remove(2);
      }
    }

    private void update_ui()
    {
      if (grepper.is_running())
      {
        set_default_widget(cancel_button);
        search_button.set_visible(false);
        cancel_button.set_visible(true);
      }
      else
      {
        set_default_widget(search_button);
        cancel_button.set_visible(false);
        search_button.set_visible(true);

        search_button.set_sensitive(search_entry.get_text().length > 0 && is_path_valid(search_path_entry.get_text()));
      }
    }

    private void add_lines_to_table(owned Array<Item> items)
    {
      item_count += items.length;
      GLib.Timeout.add(0, () =>
      {
        found_items.add_items((owned) items);
        return false;
      });
    }

    private void show_toast(string text = "", uint timeout = 2)
    {
      if (toast != null)
      {
        toast.dismiss();
        toast = null;
      }

      if(text.length > 0)
      {
        toast = new Adw.Toast(text);
        toast.set_timeout(timeout);
        toast.dismissed.connect(() =>
        {
          toast = null;
        });
        toaster.add_toast(toast);
      }
    }

    private void update_toast(string text = "", uint? timeout=null)
    {
      if (toast != null)
      {
        toast.set_title(text);
        if (timeout != null)
        {
          toast.set_timeout(timeout);
        }
      }
    }

    private void row_activated(uint position)
    {
      if (position < found_items.get_n_items())
      {
        var item = (Item) found_items.get_item(position);
        int id = 0;

        var is_flatpak = GLib.Environment.get_variable("container");
        var use_internal_preview = is_flatpak != null;

        if (use_internal_preview == false)
        {
          var editor = settings.get_string("text-editor");

          if (editor.length > 0)
          {
            if (FileUtils.test(editor, FileTest.EXISTS) == false)
            {
              var params = settings.get_string("text-editor-params");

              string[] parameters = { editor, item.file };
              if (params.length > 0)
              {
                var split = params.split(" ", 0);
                foreach (var p in split)
                {
                  if (p.index_of_char('%') > -1)
                  {
                    p = p.replace("%line%", item.line);
                  }
                  parameters += p;
                }
              }

              try
              {
                Process.spawn_async(null, parameters, null, 0, null, out id);
              }
              catch (GLib.SpawnError e)
              {
                print("error: " + e.message);
                use_internal_preview = true;
              }
            }
            else
            {
              use_internal_preview = true;
            }
          }
          else
          {
            use_internal_preview = true;
          }
        }

        if (use_internal_preview)
        {
          var preview = new Preview(this);
          preview.set_file(item.file, int.parse(item.line) - 1);
          preview.show();
        }
      }
    }
  }
}



