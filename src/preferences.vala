/* preferences.vala
 *
 * Copyright 2022 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  [GtkTemplate (ui = "/de/philippun1/Snoop/ui/preferences.ui")]
  public class Preferences : Adw.PreferencesWindow
  {
    [GtkChild]
    private unowned Gtk.Switch match_case;
    [GtkChild]
    private unowned Gtk.Switch whole_words;
    [GtkChild]
    private unowned Gtk.Switch regular_expressions;
    [GtkChild]
    private unowned Gtk.Switch ignore_hidden;
    [GtkChild]
    private unowned Gtk.Switch simple_view;

    [GtkChild]
    private unowned Gtk.Button install_extension;
    [GtkChild]
    private unowned Gtk.Button remove_extension;

    [GtkChild]
    private unowned Adw.PreferencesGroup group_nautilus_extension;
    [GtkChild]
    private unowned Adw.ActionRow nautilus_extension_status;

    [GtkChild]
    private unowned Adw.PreferencesGroup group_text_editor;
    [GtkChild]
    private unowned Gtk.Entry text_editor;
    [GtkChild]
    private unowned Gtk.Entry text_editor_params;
    [GtkChild]
    private unowned Gtk.Image text_editor_params_help;

    private Settings settings;

    private string extension_dir = "";

    public Preferences (Gtk.Application app)
    {
      settings = new Settings("de.philippun1.Snoop");

      // general
      settings.bind("match-case", match_case, "active", SettingsBindFlags.DEFAULT);
      settings.bind("whole-words", whole_words, "active", SettingsBindFlags.DEFAULT);
      settings.bind("regular-expressions", regular_expressions, "active", SettingsBindFlags.DEFAULT);
      settings.bind("ignore-hidden", ignore_hidden, "active", SettingsBindFlags.DEFAULT);
      settings.bind("simple-view", simple_view, "active", SettingsBindFlags.DEFAULT);

      var is_flatpak = GLib.Environment.get_variable("container");
      if (is_flatpak == null)
      {
        // text-editor
        settings.bind("text-editor", text_editor, "text", SettingsBindFlags.DEFAULT);
        settings.bind("text-editor-params", text_editor_params, "text", SettingsBindFlags.DEFAULT);
        text_editor_params_help.set_tooltip_text (
          _("Parameters will be added at the end.\n" +
            "The following variables are available:\n\n" +
            "%line%\tcurrent line number\n\n" +
            "i.e. for gedit you can use:" +
            "\t+%line%"
        ));

        // nautilus extension install only available in flatpak
        group_nautilus_extension.set_visible(false);
      }
      else
      {
        update_extension_status();

        // only internal snoop preview available in flatpak
        group_text_editor.set_visible(false);
      }
    }

    [GtkCallback]
    public void install_extension_clicked()
    {
      var flatpak_sandbox_dir = GLib.Environment.get_variable("FLATPAK_SANDBOX_DIR");

      var path = GLib.File.new_build_filename(extension_dir);
      if (path.query_exists() == false)
      {
        path.make_directory_with_parents(null);
      }

      var source = GLib.File.new_build_filename("/app/share/nautilus-python/extensions/snoop.py");
      var destination = GLib.File.new_build_filename(extension_dir, "snoop.py");

      if (destination.query_exists(null) == false)
      {
        source.copy(destination, GLib.FileCopyFlags.OVERWRITE);
      }

      update_extension_status();
    }

    [GtkCallback]
    public void remove_extension_clicked()
    {
      // delete extension file
      var destination = GLib.File.new_build_filename(extension_dir, "snoop.py");
      destination.delete();

      Cancellable? cancellable = null;
      var pycache_dir = GLib.File.new_build_filename(extension_dir, "__pycache__");
      FileEnumerator enumerator = pycache_dir.enumerate_children (
        "standard::*",
        FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
        cancellable);

      // delete pyc file from pycache
      FileInfo info = null;
      while (cancellable.is_cancelled () == false && ((info = enumerator.next_file (cancellable)) != null)) {
        if (info.get_file_type () == FileType.REGULAR) {
          if (info.get_name ().index_of("snoop.cpython-") == 0) {
            File file = pycache_dir.resolve_relative_path (info.get_name ());
            file.delete();
          }
        }
      }

      update_extension_status();
    }

    public void update_extension_status()
    {
      extension_dir = GLib.Environment.get_home_dir() + "/.local/share/nautilus-python/extensions";
      var extension_file = GLib.File.new_build_filename(extension_dir, "snoop.py");

      if (extension_file.query_exists(null))
      {
        nautilus_extension_status.set_subtitle(_("Extensions already installed"));
        install_extension.set_label(_("Update"));
        remove_extension.set_visible(true);
      }
      else
      {
        nautilus_extension_status.set_subtitle(_("Extension not installed yet"));
        install_extension.set_label(_("Install"));
        remove_extension.set_visible(false);
      }
    }
  }
}


