![app icon](data/icons/hicolor/scalable/apps/de.philippun1.Snoop.svg)

# Snoop

Snoop through your files

![window](img/window.png)

## Setup

There is a flatpak on flathub.
```
sudo flatpak install flathub de.philippun1.Snoop
```

To use the nautilus plugin with flatpak you can install it directly from the preferences window.

![window](img/preferences.png)

There is also a preview window which will open if you double click a row.

![window](img/preview.png)
